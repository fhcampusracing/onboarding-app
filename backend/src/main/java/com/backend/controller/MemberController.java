package com.backend.controller;

import com.backend.entity.Member;
import com.backend.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.Collection;

@RestController
@CrossOrigin("*")
@RequestMapping("/members")
public class MemberController {

    @Autowired
    private MemberService memberService;

    @RequestMapping(method = RequestMethod.GET)
    @GetMapping({"", "/"})
    public Collection<Member> getAllMembers(){
        return memberService.getAllMembers();
    }
}
