package com.backend.service;

import com.backend.entity.Member;
import com.backend.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * Member Service
 * @author michaelsvoboda
 */
@Service
public class MemberService {

    @Autowired
    @Qualifier("mysql")
    private MemberRepository memberRepository;

    public Collection<Member> getAllMembers(){
        return this.memberRepository.getAllMembers();
    }

    public Member getMemberById(int id){
        return this.memberRepository.getMemberById(id);
    }

    public void removeMemberById(int id){
        this.memberRepository.removeMemberById(id);
    }

    public void updateMember(Member member){
        this.memberRepository.updateMember(member);
    }

    public void insertMember(Member member){
        this.memberRepository.insertMemberToDB(member);
    }
}
