package com.backend.repository.impl;

import com.backend.entity.Member;
import com.backend.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

/**
 * Member MySQL Repository
 * @author michaelsvoboda
 */
@Repository("mysql")
public class MemberMySqlRepository implements MemberRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static class MemberRowMapper implements RowMapper<Member>{

        @Override
        public Member mapRow(ResultSet rs, int rowNum) throws SQLException {
            Member member = new Member();
            member.setId(rs.getInt("id"));
            member.setFirstname(rs.getString("firstname"));
            member.setLastname(rs.getString("lastname"));
            member.setEmail(rs.getString("email"));
            member.setGmail(rs.getString("gmail"));
            member.setBirthdate(rs.getDate("birthdate"));
            member.setSex(rs.getInt("sex"));
            member.setPhonenumber(rs.getString("phonenumber"));
            member.setBoarded(rs.getInt("boarded"));
            member.setEnabled(rs.getInt("enabled"));
            member.setUsername(rs.getString("username"));
            member.setPassword(rs.getString("password"));
            member.setAdmin(rs.getInt("admin"));
            member.setModuleID(rs.getInt("module_id"));

            return member;
        }
    }

    @Override
    public Collection<Member> getAllMembers() {
        final String sql = "SELECT * FROM member;";

        List<Member> members = jdbcTemplate.query(sql, new MemberRowMapper());

        return members;
    }

    @Override
    public Member getMemberById(int id) {
        return null;
    }

    @Override
    public void removeMemberById(int id) {

    }

    @Override
    public void updateMember(Member member) {

    }

    @Override
    public void insertMemberToDB(Member member) {

    }
}
