package com.backend.repository;

import com.backend.entity.Member;

import java.util.Collection;

/**
 * Member Repository Interface
 * @author michaelsvoboda
 */
public interface MemberRepository {

    Collection<Member> getAllMembers();

    Member getMemberById(int id);

    void removeMemberById(int id);

    void updateMember(Member member);

    void insertMemberToDB(Member member);

}
