package com.backend.entity;

import java.util.Date;

/**
 * Member Entity
 * @author michaelsvoboda
 */
public class Member extends BasicEntity {
    /**
     * Firstname
     */
    private String firstname;
    /**
     * Lastname
     */
    private String lastname;
    /**
     * Email Address
     */
    private String email;
    /**
     * Gmail Address
     */
    private String gmail;
    /**
     * Birthdate
     */
    private Date birthdate;
    /**
     * Sex Flag
     * 0 = men, 1 = women
     */
    private int sex;
    /**
     * Phonenumber
     */
    private String phonenumber;
    /**
     * Boarded flag
     * 0 = not boarded, 1 = boarded
     */
    private int boarded;
    /**
     * Enabled flag
     * 0 = disabled, 1 = enabled
     */
    private int enabled;
    /**
     * Username
     */
    private String username;
    /**
     * Password
     */
    private String password;
    /**
     * Admin flag
     * 0 = no admin, 1 = admin
     */
    private int admin;
    /**
     * Module ID (Foreign Key)
     */
    private int moduleID;

    /**
     * constructor
     * @param id
     * @param firstname
     * @param lastname
     * @param email
     * @param gmail
     * @param birthdate
     * @param sex
     * @param phonenumber
     * @param boarded
     * @param enabled
     * @param username
     * @param password
     * @param admin
     * @param moduleID
     */
    public Member(int id, String firstname, String lastname, String email, String gmail, Date birthdate, int sex, String phonenumber, int boarded, int enabled, String username, String password, int admin, int moduleID) {
        super(id);
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.gmail = gmail;
        this.birthdate = birthdate;
        this.sex = sex;
        this.phonenumber = phonenumber;
        this.boarded = boarded;
        this.enabled = enabled;
        this.username = username;
        this.password = password;
        this.admin = admin;
        this.moduleID = moduleID;
    }

    /**
     * basic constructor
     */
    public Member(){}

    /**
     * Get Firstname
     * @return
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * Set Firstname
     * @param firstname
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * Get Lastname
     * @return
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * Set Lastname
     * @param lastname
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * Get Email
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set Email
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Get Gmail
     * @return
     */
    public String getGmail() {
        return gmail;
    }

    /**
     * Set Gmail
     * @param gmail
     */
    public void setGmail(String gmail) {
        this.gmail = gmail;
    }

    /**
     * Get Birthdate
     * @return
     */
    public Date getBirthdate() {
        return birthdate;
    }

    /**
     * Set Birthdate
     * @param birthdate
     */
    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    /**
     * Get Sex Flag
     * @return
     */
    public int getSex() {
        return sex;
    }

    /**
     * Set Sex flag
     * @param sex
     */
    public void setSex(int sex) {
        this.sex = sex;
    }

    /**
     * Get Phonenumber
     * @return
     */
    public String getPhonenumber() {
        return phonenumber;
    }

    /**
     * Set Phonenumber
     * @param phonenumber
     */
    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    /**
     * Get Boarded flag
     * @return
     */
    public int getBoarded() {
        return boarded;
    }

    /**
     * Set Boarded flag
     * @param boarded
     */
    public void setBoarded(int boarded) {
        this.boarded = boarded;
    }

    /**
     * Get Enabled Flag
     * @return
     */
    public int getEnabled() {
        return enabled;
    }

    /**
     * Set Enabled Flag
     * @param enabled
     */
    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    /**
     * Get Username
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set Username
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Get Password
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set Password
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Get admin flag
     * @return
     */
    public int getAdmin() {
        return admin;
    }

    /**
     * Set admin flag
     * @param admin
     */
    public void setAdmin(int admin) {
        this.admin = admin;
    }

    /**
     * set module id
     * @return
     */
    public int getModuleID() {
        return moduleID;
    }

    /**
     * get module id
     * @param moduleID
     */
    public void setModuleID(int moduleID) {
        this.moduleID = moduleID;
    }
}
