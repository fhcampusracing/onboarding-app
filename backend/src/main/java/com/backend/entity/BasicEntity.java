package com.backend.entity;

/**
 * Basic Entity class
 * @author michaelsvoboda
 */
public class BasicEntity {

    private int id;

    /**
     * constructor
     * @param id
     */
    public BasicEntity(int id){
        this.id = id;
    }

    /**
     * default constructor
     */
    public BasicEntity(){}

    /**
     * get ID
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * set ID
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }
}
